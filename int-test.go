package main

import (
	dbm "bitbucket.org/tsiemens/bridgescoreserver/db"
	"bitbucket.org/tsiemens/bridgescoreserver/inttest"

	"flag"
	"fmt"
	"log"
	"os"
)

var credsFile string
var clear bool

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, `Usage of int-test: [-c CREDS] [TEST]
tests: smoke (default), null

`)
		flag.PrintDefaults()
	}
	flag.StringVar(&credsFile, "c", "inttest/mysql_creds.txt",
		"Test database credentials file")
	flag.BoolVar(&clear, "clear", false, "Clear the db contents before testing")
	flag.Parse()

	args := flag.Args()

	testName := "smoke"
	if len(args) > 1 {
		fmt.Fprintf(os.Stderr, "Too many arguments\n")
		flag.Usage()
		os.Exit(2)
	}

	if len(args) >= 1 {
		testName = args[0]
	}

	conn, err := dbm.OpenMysqlFromConfigFile(credsFile)
	if err != nil {
		log.Fatal(err)
		return
	}
	db := dbm.NewMysqlDb(conn)
	defer db.Close()

	if clear {
		inttest.ClearDb(db)
	}

	fmt.Println("Running:", testName)
	switch testName {
	case "smoke":
		inttest.SmokeTest(db)
	case "null":
		// Do nothing
	default:
		fmt.Fprintf(os.Stderr, "Invalid test name\n")
		flag.Usage()
		os.Exit(2)
	}
	fmt.Println("ok")
}
