package model

type JsonSessionList struct {
	Sessions []SessionBase
}

type JsonSessionId struct {
	SessionId SessionId
}

type JsonNewSessionReq struct {
	Password string
	// SessionPassword
	Session Session
}

type JsonDelSessionReq struct {
	Password string
	// SessionPassword
	SessionId SessionId
}

type JsonBoardSets struct {
	BoardSets []BoardSet
}

type JsonBoardSetUpdateReq struct {
	Password  string
	SessionId SessionId
	BoardSet  BoardSet
}

type JsonErrorContainer struct {
	Error string
}
