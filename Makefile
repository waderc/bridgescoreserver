
all: build

build:
	go build bridgescorefcgi.go

run:
	go run bridgescorefcgi.go

testdeps:
	go get gopkg.in/DATA-DOG/go-sqlmock.v1

test:
	go test ./test

clean:
	rm bridgescorefcgi

.PHONY: clean test testdeps run
