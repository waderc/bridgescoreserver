package inttest

import (
	dbm "bitbucket.org/tsiemens/bridgescoreserver/db"
	"bitbucket.org/tsiemens/bridgescoreserver/model"

	"database/sql"
	"fmt"
	"log"
	"reflect"
)

const (
	sampleDate   = "2017-01-02 15:04:05"
	sampleDateTs = 1483369445000
)

func ClearDb(db *dbm.MysqlDb) {
	fmt.Println("Clearing db")
	db.Transact(func(tx *sql.Tx) error {
		// This should cascade
		_, err := tx.Exec("DELETE FROM Session")
		if err != nil {
			fmt.Println("Error:", err)
		}
		return err
	})
}

func updateAndCheckBoardSet(
	db dbm.Database, boardSet *model.BoardSet, sessId model.SessionId) {

	err := db.AddOrUpdateBoardSet(boardSet, sessId)
	if err != nil {
		log.Fatal(err)
	}

	boardSets, err := db.GetSessionBoardSets(sessId)
	if err != nil {
		log.Fatal(err)
	}
	if len(boardSets) != 1 {
		log.Fatalf("Found %d board sets for session %d", len(boardSets), sessId)
	}
	if len(boardSets[0].Boards) != len(boardSet.Boards) {
		log.Fatalf("Found %d boards for set", len(boardSets[0].Boards))
	}

	if !reflect.DeepEqual(boardSets[0], *boardSet) {
		log.Fatalf("Not equal.\nlocal:%+v,\ndb:   %+v", boardSets[0], *boardSet)
	}
}

func SmokeTest(db dbm.Database) {
	// Create session
	sess := model.NewSession(0, "Sess 1", sampleDateTs, "")
	err := db.AddSession(&sess)
	if err != nil {
		log.Fatal(err)
	}

	sessions, err := db.GetSessions()
	if err != nil {
		log.Fatal(err)
	}

	if len(sessions) == 0 {
		log.Fatal("No sessions in DB")
	} else if len(sessions) > 1 {
		fmt.Printf("More sessions found than expected: %d\n", len(sessions))
	}

	var gotSess *model.Session
	for i, s := range sessions {
		if s.Id == sess.Id {
			gotSess = &sessions[i]
		}
	}

	if gotSess == nil {
		log.Fatal("Didn't find session")
	}

	if !reflect.DeepEqual(sess, *gotSess) {
		log.Fatalf("Not equal. local:%+v, db:%+v", sess, *gotSess)
	}

	// Seconary get method
	gotSess2, ok, err := db.GetSession(sess.Id)
	if err != nil {
		log.Fatal(err)
	} else if !ok {
		log.Fatal("Didn't find session (second time)")
	}

	if !reflect.DeepEqual(sess, gotSess2) {
		log.Fatalf("Not equal. local:%+v, db:%+v", sess, gotSess2)
	}

	// Create BoardSet and Boards
	boardSet := model.BoardSet{Team: "Bob", PublisherId: "someid"}
	boardSet.Boards = append(boardSet.Boards, model.Board{
		BoardNumber:   1,
		Suit:          model.Hearts,
		Level:         2,
		Doubled:       model.Doubled,
		Tricks:        3,
		Declarer:      model.South,
		PublisherSeat: model.North,
	})
	updateAndCheckBoardSet(db, &boardSet, sess.Id)

	// fmt.Printf("%+v\n", boardSet)

	// Update board and set
	boardSet.Id = 0 // Reset
	boardSet.Team = "Joe"
	boardSet.Boards[0] = model.Board{
		BoardNumber:   1,
		Suit:          model.Spades,
		Level:         3,
		Doubled:       model.Undoubled,
		Tricks:        4,
		Declarer:      model.East,
		PublisherSeat: model.West,
	}
	updateAndCheckBoardSet(db, &boardSet, sess.Id)

	// fmt.Printf("%+v\n", boardSet)

	// Delete session
	err = db.DelSession(sess.Id)
	if err != nil {
		log.Fatal(err)
	}
	sessions, err = db.GetSessions()
	if err != nil {
		log.Fatal(err)
	}

	for _, s := range sessions {
		if s.Id == sess.Id {
			log.Fatal("Still found session")
		}
	}
}
