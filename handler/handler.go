package handler

import (
	"bitbucket.org/tsiemens/bridgescoreserver/apiutil"
	"bitbucket.org/tsiemens/bridgescoreserver/db"
	"bitbucket.org/tsiemens/bridgescoreserver/model"
	"bitbucket.org/tsiemens/bridgescoreserver/util"
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"strconv"
	"sync"
)

const (
	pathEnd = `($|/|\?|#)`
)

var sessionsUrlRe = regexp.MustCompile("/sessions" + pathEnd)
var newSessionUrlRe = regexp.MustCompile("/newsession" + pathEnd)
var delSessionUrlRe = regexp.MustCompile("/delsession" + pathEnd)
var boardsUrlRe = regexp.MustCompile("/boards" + pathEnd)

type Handler struct {
	lock     sync.RWMutex
	Password *util.Password
	Db       db.Database
}

func NewHandler(password *util.Password, database db.Database) *Handler {
	return &Handler{
		Password: password,
		Db:       database,
	}
}

func WriteJson(resp http.ResponseWriter, code int, o interface{}) bool {
	err := apiutil.WriteJsonToResponse(resp, code, o)
	return err == nil
}

func WriteJsonDbError(w http.ResponseWriter, err error) error {
	return apiutil.WriteJsonError(w, 500, fmt.Sprintf("%s", err))
}

func (h *Handler) AuthCheck(pass string) bool {
	if h.Password != nil {
		return h.Password.Validate(pass)
	} else {
		return true
	}
}

func (h *Handler) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	resp.Header().Set("Cache-Control", "no-store")
	resp.Header().Set("Access-Control-Allow-Origin", "*")
	resp.Header().Set("Content-Type", "application/json")
	m := sessionsUrlRe.MatchString(req.URL.Path)
	if m {
		h.ServeSessions(resp, req)
		return
	}
	m = newSessionUrlRe.MatchString(req.URL.Path)
	if m {
		h.ServeNewSession(resp, req)
		return
	}
	m = delSessionUrlRe.MatchString(req.URL.Path)
	if m {
		h.ServeDelSession(resp, req)
		return
	}
	m = boardsUrlRe.MatchString(req.URL.Path)
	if m {
		h.ServeBoards(resp, req)
		return
	}

	var buf bytes.Buffer
	resp.WriteHeader(404)
	buf.WriteString(fmt.Sprintf("<p>%v is not a valid endpoint</p>", req.URL.Path))
	resp.Write(buf.Bytes())
}

// GET /sessions
func (h *Handler) ServeSessions(resp http.ResponseWriter, req *http.Request) {
	if req.Method == "GET" {
		var sessions model.JsonSessionList
		h.lock.RLock()
		dbSessions, err := h.Db.GetSessions()
		h.lock.RUnlock()
		if err != nil {
			WriteJsonDbError(resp, err)
			return
		}
		sessions.Sessions = make([]model.SessionBase, 0, len(dbSessions))
		for _, sess := range dbSessions {
			sessions.Sessions = append(sessions.Sessions, sess.SessionBase)
		}
		WriteJson(resp, 200, sessions)
	} else {
		apiutil.WriteJsonError(resp, 400, "GET only")
	}
}

// POST /newsession
func (h *Handler) ServeNewSession(resp http.ResponseWriter, req *http.Request) {
	if req.Method == "POST" {
		dec := json.NewDecoder(req.Body)
		var newSessReq model.JsonNewSessionReq
		err := dec.Decode(&newSessReq)
		req.Body.Close()
		if err != nil {
			apiutil.WriteJsonError(resp, 400, err.Error())
		} else {
			if !h.AuthCheck(newSessReq.Password) {
				apiutil.WriteJsonError(resp, 403, "Authentication failed")
				return
			}
			h.lock.Lock()
			newSessReq.Session.Id = 0
			err = h.Db.AddSession(&newSessReq.Session)
			h.lock.Unlock()
			if err != nil {
				WriteJsonDbError(resp, err)
				return
			}

			jsonSessId := model.JsonSessionId{newSessReq.Session.Id}
			WriteJson(resp, 201, jsonSessId)
		}
	} else {
		apiutil.WriteJsonError(resp, 400, "POST only")
	}
}

// TODO refactor out common parse and auth code
// POST /delsession
func (h *Handler) ServeDelSession(resp http.ResponseWriter, req *http.Request) {
	if req.Method == "POST" {
		dec := json.NewDecoder(req.Body)
		var delSessReq model.JsonDelSessionReq
		err := dec.Decode(&delSessReq)
		req.Body.Close()
		if err != nil {
			apiutil.WriteJsonError(resp, 400, err.Error())
		} else {
			if !h.AuthCheck(delSessReq.Password) {
				apiutil.WriteJsonError(resp, 403, "Authentication failed")
				return
			}
			if delSessReq.SessionId == 0 {
				apiutil.WriteJsonError(resp, 400, "Valid SessionId required")
				return
			}
			// TODO check session password
			h.lock.Lock()
			defer h.lock.Unlock()
			err = h.Db.DelSession(delSessReq.SessionId)
			if err != nil {
				WriteJsonDbError(resp, err)
				return
			}
		}
	} else {
		apiutil.WriteJsonError(resp, 400, "POST only")
	}
}

// GET /boards?sessionId=x
// POST /boards
func (h *Handler) ServeBoards(resp http.ResponseWriter, req *http.Request) {
	if req.Method == "GET" {
		h.serveGetBoards(resp, req)
	} else if req.Method == "POST" {
		h.serveUpdateBoards(resp, req)
	} else {
		apiutil.WriteJsonError(resp, 400, "GET and POST only")
	}
}

func (h *Handler) serveGetBoards(resp http.ResponseWriter, req *http.Request) {
	sessId, err := strconv.ParseInt(req.URL.Query().Get("sessionId"), 10, 64)
	if err != nil {
		apiutil.WriteJsonError(resp, 400, "Invalid sessionId")
		return
	}
	var boardSets model.JsonBoardSets
	h.lock.RLock()
	defer h.lock.RUnlock()
	_, ok, err := h.Db.GetSession(model.SessionId(sessId))
	if err != nil {
		WriteJsonDbError(resp, err)
		return
	} else if !ok {
		apiutil.WriteJsonError(resp, 404, fmt.Sprintf("No SessionId %d", sessId))
		return
	}

	boardSetsForSession, err := h.Db.GetSessionBoardSets(model.SessionId(sessId))
	if err != nil {
		WriteJsonDbError(resp, err)
		return
	}

	boardSets.BoardSets = make([]model.BoardSet, 0, len(boardSetsForSession))
	for _, boardSet := range boardSetsForSession {
		boardSets.BoardSets = append(boardSets.BoardSets, boardSet)
	}
	WriteJson(resp, 200, boardSets)
}

func (h *Handler) serveUpdateBoards(resp http.ResponseWriter, req *http.Request) {
	dec := json.NewDecoder(req.Body)
	var updateReq model.JsonBoardSetUpdateReq
	err := dec.Decode(&updateReq)
	req.Body.Close()
	if err != nil {
		apiutil.WriteJsonError(resp, 400, err.Error())
	} else {
		if !h.AuthCheck(updateReq.Password) {
			apiutil.WriteJsonError(resp, 403, "Authentication failed")
			return
		}
		sessId := updateReq.SessionId
		if sessId == 0 {
			apiutil.WriteJsonError(resp, 400, fmt.Sprintf("Invalid SessionId"))
			return
		}
		privatePublisherId := updateReq.BoardSet.PublisherId
		if privatePublisherId == "" {
			apiutil.WriteJsonError(resp, 400, fmt.Sprintf("Invalid PublisherId"))
			return
		}
		if updateReq.BoardSet.Team == "" {
			apiutil.WriteJsonError(resp, 400, fmt.Sprintf("Team required"))
			return
		}

		if updateReq.BoardSet.Boards == nil {
			apiutil.WriteJsonError(resp, 400, "Invalid BoardSet")
			return
		}

		for _, board := range updateReq.BoardSet.Boards {
			if ok, err := board.IsValid(); !ok {
				apiutil.WriteJsonError(resp, 400, err.Error())
				return
			}
		}

		publisherId := privatePublisherId.PrivateToPublic()
		updateReq.BoardSet.PublisherId = publisherId
		h.lock.Lock()
		defer h.lock.Unlock()
		_, ok, err := h.Db.GetSession(sessId)
		if err != nil {
			WriteJsonDbError(resp, err)
			return
		} else if !ok {
			apiutil.WriteJsonError(resp, 404, fmt.Sprintf("No SessionId %d", sessId))
		} else {
			err := h.Db.AddOrUpdateBoardSet(&updateReq.BoardSet, sessId)
			if err != nil {
				WriteJsonDbError(resp, err)
			}
		}
	}
}
