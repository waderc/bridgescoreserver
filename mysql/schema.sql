CREATE TABLE Session (
   id INTEGER NOT NULL AUTO_INCREMENT,
   name VARCHAR(512),
   timestamp DATETIME NOT NULL,
   password VARCHAR(1024),

   PRIMARY KEY (id)
);

CREATE TABLE BoardSet (
   id INTEGER NOT NULL AUTO_INCREMENT,
   sessionId INTEGER NOT NULL,
   publisherId VARCHAR(512) NOT NULL,
   team VARCHAR(512),

   FOREIGN KEY (sessionId)
      REFERENCES Session(id)
      ON DELETE CASCADE,

   PRIMARY KEY (id),
   UNIQUE (sessionId, publisherId)
);

CREATE TABLE Board (
   boardSetId INTEGER NOT NULL,
   boardNumber INTEGER NOT NULL,
   suit VARCHAR(48) NOT NULL,
   level INTEGER NOT NULL,
   doubled VARCHAR(48),
   tricks INTEGER,
   declarer VARCHAR(48),
   publisherSeat VARCHAR(48) NOT NULL,

   FOREIGN KEY (boardSetId)
      REFERENCES BoardSet(id)
      ON DELETE CASCADE,

   PRIMARY KEY (boardSetId, boardNumber)
);
