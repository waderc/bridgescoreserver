#!/usr/bin/env python

import datetime
import sys
import requests

host = sys.argv[ 1 ]
urlStart = 'http://' + host

r = requests.get( urlStart + '/sessions' )
sessions = r.json()

def tsToDt( ts ):
   return datetime.datetime.fromtimestamp( ts / 1000 ).strftime( '%Y-%m-%d %H:%M:%S' )

boardSetId = 1

for sess in sessions[ 'Sessions' ][ :]:
   # print sess
   assert '"' not in sess[ 'Name' ]
   print """INSERT INTO Session (id, name, timestamp)
   VALUES(%d, "%s", '%s');""" % ( sess[ 'Id' ], sess[ 'Name' ],
                                  tsToDt( sess[ 'Timestamp' ] ) )
   r = requests.get( urlStart + '/boards?sessionId=' + str( sess[ 'Id' ] ) )
   boardsJson = r.json()
   import pprint
   # pprint.pprint( boardsJson )
   # print boards
   for boardSet in boardsJson[ 'BoardSets' ]:
      assert '"' not in boardSet[ 'Team' ]
      print """INSERT INTO BoardSet (id, sessionId, team, publisherId)
   VALUES(%d, %d, "%s", "%s");""" % ( boardSetId, sess[ 'Id' ],
                                     boardSet[ 'Team' ], boardSet[ 'PublisherId' ] )

      for board in boardSet[ 'Boards' ]:
         print """INSERT INTO Board (boardSetId, boardNumber, suit, level, doubled,
   tricks, declarer, publisherSeat)
   VALUES(%d, %d, "%s", %d, "%s", %d, "%s", "%s");""" % \
         ( boardSetId, board[ 'BoardNumber' ], board[ 'Suit' ],
           board[ 'Level' ], board[ 'Doubled' ], board[ 'Tricks' ],
           board[ 'Declarer' ], board[ 'PublisherSeat' ] )

      boardSetId += 1

   print ''
