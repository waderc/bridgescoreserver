package util

import (
	"crypto/sha1"
	"encoding/hex"
	"errors"
	"io"
)

type Sha1Hash [sha1.Size]byte

func Sha1HashString(str string) Sha1Hash {
	h := sha1.New()
	io.WriteString(h, str)
	hashSlice := h.Sum(nil)
	var hash Sha1Hash
	copy(hash[:], hashSlice[:])
	return hash
}

func Sha1HashStringToString(str string) string {
	h := Sha1HashString(str)
	return hex.EncodeToString(h[:])
}

func HashPassword(salt string, password string) Sha1Hash {
	h := sha1.New()
	io.WriteString(h, salt)
	io.WriteString(h, password)
	hashSlice := h.Sum(nil)
	var hash Sha1Hash
	copy(hash[:], hashSlice[:])
	return hash
}

func HexToHash(hexStr string) (Sha1Hash, error) {
	var hash Sha1Hash
	hashSlice, err := hex.DecodeString(hexStr)
	if err != nil {
		return hash, err
	}
	if len(hashSlice) != len(hash) {
		return hash, errors.New("Invalid hash length")
	}
	copy(hash[:], hashSlice)
	return hash, nil
}

type Password struct {
	Salt string
	Sha1 Sha1Hash
}

func NewPasswordFromString(salt string, password string) *Password {
	return &Password{Salt: salt, Sha1: HashPassword(salt, password)}
}

func (p *Password) Validate(password string) bool {
	return HashPassword(p.Salt, password) == p.Sha1
}
