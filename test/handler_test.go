package test

import (
	"bitbucket.org/tsiemens/bridgescoreserver/handler"
	"bitbucket.org/tsiemens/bridgescoreserver/model"
	"bitbucket.org/tsiemens/bridgescoreserver/util"
	"bytes"
	"fmt"
	"net/http"
	"strings"
	"testing"
)

const (
	password = "Foo"
	salt     = "pinchofsalt"

	sha1Ofx = "11f6ad8ec52a2984abaafd7c3b516503785c2072"
	sha1Ofy = "95cb0bfd2977c761298d9624e4b4d4c72a39974a"
)

type MockResponseWriter struct {
	header http.Header
	code   *int
	buf    *bytes.Buffer
}

func NewMockResponseWriter() MockResponseWriter {
	hdr := make(http.Header)
	code := new(int)
	*code = 200
	return MockResponseWriter{
		header: hdr,
		code:   code,
		buf:    new(bytes.Buffer),
	}
}

func (w MockResponseWriter) Header() http.Header {
	return w.header
}

func (w MockResponseWriter) Write(b []byte) (int, error) {
	return w.buf.Write(b)
}

func (w MockResponseWriter) WriteHeader(code int) {
	*w.code = code
}

func ServeRequest(h *handler.Handler, action string, url string, body string) (string, int) {
	writer := NewMockResponseWriter()
	bodyReader := strings.NewReader(body)
	req, err := http.NewRequest(action, url, bodyReader)
	if err != nil {
		panic(err)
	}
	h.ServeHTTP(writer, req)
	return strings.TrimSpace(writer.buf.String()), *writer.code
}

func NewTestHandler() (*handler.Handler, *MockDb) {
	mockDb := NewMockDb()
	return handler.NewHandler(util.NewPasswordFromString(salt, password), mockDb),
		mockDb
}

func NewTestHandlerNoPass() (*handler.Handler, *MockDb) {
	mockDb := NewMockDb()
	return handler.NewHandler(nil, mockDb), mockDb
}

func TestBadPath(t *testing.T) {
	h, _ := NewTestHandler()

	_, code := ServeRequest(h, "GET", "/sessionsiii", "")
	AssertIntsEqual(t, code, 404)
}

func TestGetSessions(t *testing.T) {
	h, db := NewTestHandler()
	db.Sessions[1] = model.Session{SessionBase: model.SessionBase{Id: 1, Name: "Foo", Timestamp: 50}}

	ret, code := ServeRequest(h, "GET", "/sessions", "")
	AssertIntsEqual(t, code, 200)
	AssertStringsEqual(t, ret, `{"Sessions":[{"Id":1,"Name":"Foo","Timestamp":50}]}`)

}

func TestGetNoSessions(t *testing.T) {
	h, _ := NewTestHandler()

	ret, code := ServeRequest(h, "GET", "/sessions", "")
	AssertIntsEqual(t, code, 200)
	AssertStringsEqual(t, ret, `{"Sessions":[]}`)
}

func TestNewSessions(t *testing.T) {
	h, db := NewTestHandlerNoPass()

	ret, code := ServeRequest(h, "POST", "/newsession",
		`{"Session":{"Id":6,"Name":"Foo","Timestamp":42}}`)
	AssertIntsEqual(t, code, 201)
	AssertStringsEqual(t, ret, `{"SessionId":1}`)

	ret, code = ServeRequest(h, "POST", "/newsession",
		`{"Session":{"Name":"Bar","Timestamp":43, "Password":"pwd"}}`)
	AssertIntsEqual(t, code, 201)
	AssertStringsEqual(t, ret, `{"SessionId":2}`)

	AssertIntsEqual(t, len(db.Sessions), 2)
	expect := model.Session{SessionBase: model.SessionBase{Id: 1, Name: "Foo", Timestamp: 42}}
	AssertEqual(t, db.Sessions[1], expect)

	expect = model.Session{
		SessionBase: model.SessionBase{Id: 2, Name: "Bar", Timestamp: 43},
		Password:    "pwd"}
	AssertEqual(t, db.Sessions[2], expect)
}

func TestNewSessionAuth(t *testing.T) {
	h, db := NewTestHandler()
	authErr := `{"Error":"Authentication failed"}`

	ret, code := ServeRequest(h, "POST", "/newsession",
		`{"Session":{"Id":6,"Name":"Bla","Timestamp":42}}`)
	AssertIntsEqual(t, code, 403)
	AssertStringsEqual(t, ret, authErr)
	AssertIntsEqual(t, len(db.Sessions), 0)

	ret, code = ServeRequest(h, "POST", "/newsession",
		`{"Password":"Foop", "Session":{"Id":6,"Name":"Bla","Timestamp":42}}`)
	AssertIntsEqual(t, code, 403)
	AssertStringsEqual(t, ret, authErr)
	AssertIntsEqual(t, len(db.Sessions), 0)

	ret, code = ServeRequest(h, "POST", "/newsession",
		`{"Password":"Foo","Session":{"Name":"Bar","Timestamp":42}}`)
	AssertIntsEqual(t, code, 201)
	AssertStringsEqual(t, ret, `{"SessionId":1}`)

	AssertIntsEqual(t, len(db.Sessions), 1)
	expect := model.Session{SessionBase: model.SessionBase{Id: 1, Name: "Bar", Timestamp: 42}}
	AssertEqual(t, db.Sessions[1], expect)
}

func TestNewSessionsInvalidInput(t *testing.T) {
	h, _ := NewTestHandler()

	_, code := ServeRequest(h, "POST", "/newsession", `{"Session":}`)
	AssertIntsEqual(t, code, 400)
}

func TestDelSession(t *testing.T) {
	h, db := NewTestHandlerNoPass()

	db.Sessions[1] = model.Session{SessionBase: model.SessionBase{
		Id: 1, Name: "Foo", Timestamp: 42}}
	db.Boards[1] = map[model.PublisherId]model.BoardSet{}
	db.Sessions[3] = model.Session{SessionBase: model.SessionBase{
		Id: 3, Name: "Bar", Timestamp: 42}}
	db.Boards[3] = map[model.PublisherId]model.BoardSet{}

	_, code := ServeRequest(h, "POST", "/delsession", `{"SessionId": 3}`)
	AssertIntsEqual(t, code, 200)
	AssertIntsEqual(t, len(db.Sessions), 1)
	_, ok := db.Sessions[3]
	AssertEqual(t, ok, false)
	_, ok = db.Boards[3]
	AssertEqual(t, ok, false)
}

func TestDelSessionInvalidInput(t *testing.T) {
	h, db := NewTestHandlerNoPass()

	db.Sessions[1] = model.Session{SessionBase: model.SessionBase{
		Id: 1, Name: "Foo", Timestamp: 42}}

	_, code := ServeRequest(h, "POST", "/delsession", `{"SessionId": "1"}`)
	AssertIntsEqual(t, code, 400)
	AssertIntsEqual(t, len(db.Sessions), 1)

	_, code = ServeRequest(h, "POST", "/delsession", `{"SessionId": 1`)
	AssertIntsEqual(t, code, 400)
	AssertIntsEqual(t, len(db.Sessions), 1)

	_, code = ServeRequest(h, "POST", "/delsession", `{}`)
	AssertIntsEqual(t, code, 400)
	AssertIntsEqual(t, len(db.Sessions), 1)

	_, ok := db.Sessions[1]
	AssertTrue(t, ok)
}

func TestDelSessionAuth(t *testing.T) {
	h, db := NewTestHandler()

	db.Sessions[1] = model.Session{SessionBase: model.SessionBase{
		Id: 1, Name: "Foo", Timestamp: 42}}

	_, code := ServeRequest(h, "POST", "/delsession", `{"SessionId": 1}`)
	AssertIntsEqual(t, code, 403)
	AssertIntsEqual(t, len(db.Sessions), 1)

	_, code = ServeRequest(h, "POST", "/delsession", `{"Password":"foo","SessionId": 1}`)
	AssertIntsEqual(t, code, 403)
	AssertIntsEqual(t, len(db.Sessions), 1)

	_, code = ServeRequest(h, "POST", "/delsession", `{"Password":"Foo","SessionId": 1}`)
	AssertIntsEqual(t, code, 200)
	AssertIntsEqual(t, len(db.Sessions), 0)
}

func NewTestBoardSet(x model.PublisherId, boards []model.Board) model.BoardSet {
	return model.BoardSet{
		Team:        "Team" + string(x),
		PublisherId: x.PrivateToPublic(),
		Boards:      boards,
	}
}

func NewTestBoard(i int) model.Board {
	return model.Board{
		BoardNumber:   i,
		Suit:          model.Spades,
		Level:         4,
		Doubled:       "undoubled",
		Tricks:        5,
		Declarer:      "north",
		PublisherSeat: "east",
	}
}

func TestGetBoards(t *testing.T) {
	h, db := NewTestHandler()

	db.Sessions[1] = model.Session{SessionBase: model.SessionBase{
		Id: 1, Name: "Foo", Timestamp: 42}}
	db.Sessions[2] = model.Session{SessionBase: model.SessionBase{
		Id: 2, Name: "Bar", Timestamp: 42}}

	db.Boards[1] = map[model.PublisherId]model.BoardSet{
		sha1Ofx: NewTestBoardSet("x", []model.Board{
			NewTestBoard(1),
		}),
	}
	db.Boards[2] = map[model.PublisherId]model.BoardSet{
		sha1Ofx: NewTestBoardSet("x", []model.Board{
			NewTestBoard(1),
			NewTestBoard(2),
		}),
		sha1Ofy: NewTestBoardSet("y", []model.Board{
			NewTestBoard(1),
			NewTestBoard(2),
		}),
	}

	ret, code := ServeRequest(h, "GET", "/boards?sessionId=1", "")
	AssertIntsEqual(t, 200, code)
	AssertStringsEqual(t, ret, `{"BoardSets":[{"Id":0,"Team":"Teamx","PublisherId":"`+sha1Ofx+`",`+
		`"Boards":[{"BoardNumber":1,"Suit":"spades","Level":4,`+
		`"Doubled":"undoubled","Tricks":5,"Declarer":"north","PublisherSeat":"east"}]}]}`)

	ret, code = ServeRequest(h, "GET", "/boards?sessionId=2", "")
	AssertIntsEqual(t, 200, code)
	AssertIntsEqual(t, strings.Count(ret, "PublisherId"), 2)
	AssertIntsEqual(t, strings.Count(ret, "BoardNumber"), 4)
}

func TestGetBoardsInvalid(t *testing.T) {
	h, db := NewTestHandler()

	db.Sessions[1] = model.Session{SessionBase: model.SessionBase{
		Id: 1, Name: "Foo", Timestamp: 42}}

	db.Boards[1] = map[model.PublisherId]model.BoardSet{
		sha1Ofx: NewTestBoardSet("x", []model.Board{
			NewTestBoard(1),
		}),
	}

	ret, code := ServeRequest(h, "GET", "/boards?sessionId=2", "")
	AssertIntsEqual(t, 404, code)
	AssertStringsEqual(t, ret, `{"Error":"No SessionId 2"}`)

	ret, code = ServeRequest(h, "GET", "/boards", "")
	AssertIntsEqual(t, 400, code)
}

func TestUpdateBoards(t *testing.T) {
	h, db := NewTestHandlerNoPass()
	db.Sessions[1] = model.Session{SessionBase: model.SessionBase{
		Id: 1, Name: "Foo", Timestamp: 42}}
	db.Boards[1] = map[model.PublisherId]model.BoardSet{}

	_, code := ServeRequest(h, "POST", "/boards",
		`{"SessionId": 1,
		"BoardSet":{"Team":"Teamx","PublisherId":"x",
		"Boards":[{"BoardNumber":1,"Suit":"none","Level":0,
		"Doubled":"undoubled","Tricks":0,"Declarer":"none","PublisherSeat":"east"}]}}`)
	AssertIntsEqual(t, code, 200)
	testBoard := model.Board{
		BoardNumber:   1,
		Suit:          model.None,
		Level:         0,
		Doubled:       "undoubled",
		Tricks:        0,
		Declarer:      model.None,
		PublisherSeat: model.East,
	}
	AssertEqual(t, db.Boards[1][sha1Ofx].Boards[0], testBoard)

	_, code = ServeRequest(h, "POST", "/boards",
		`{"SessionId": 1,
		"BoardSet":{"Team":"Teamx","PublisherId":"x",
		"Boards":[{"BoardNumber":1,"Suit":"spades","Level":4,
		"Doubled":"undoubled","Tricks":5,"Declarer":"north","PublisherSeat":"east"}]}}`)
	AssertIntsEqual(t, code, 200)
	testBoard = NewTestBoard(1)
	AssertEqual(t, db.Boards[1][sha1Ofx].Boards[0], testBoard)

	_, code = ServeRequest(h, "POST", "/boards",
		`{"SessionId": 1,
		"BoardSet":{"Team":"Teamx","PublisherId":"x",
		"Boards":[{"BoardNumber":1,"Suit":"hearts","Level":4,
		"Doubled":"undoubled","Tricks":5,"Declarer":"north","PublisherSeat":"east"}]}}`)
	AssertIntsEqual(t, code, 200)
	testBoard.Suit = model.Hearts
	AssertIntsEqual(t, len(db.Boards[1][sha1Ofx].Boards), 1)
	AssertEqual(t, db.Boards[1][sha1Ofx].Boards[0], testBoard)
}

func TestUpdateBoardsAuth(t *testing.T) {
	h, db := NewTestHandler()
	db.Sessions[1] = model.Session{SessionBase: model.SessionBase{
		Id: 1, Name: "Foo", Timestamp: 42}}
	db.Boards[1] = map[model.PublisherId]model.BoardSet{}

	updateStr := `{%s"SessionId": 1,
		"BoardSet":{"Team":"Teamx","PublisherId":"x",
		"Boards":[{"BoardNumber":1,"Suit":"spades","Level":4,
		"Doubled":"undoubled","Tricks":5,"Declarer":"north","PublisherSeat":"east"}]}}`
	_, code := ServeRequest(h, "POST", "/boards", fmt.Sprintf(updateStr, ""))

	AssertIntsEqual(t, code, 403)
	AssertEqual(t, len(db.Boards[1][sha1Ofx].Boards), 0)

	_, code = ServeRequest(h, "POST", "/boards", fmt.Sprintf(updateStr, `"Password":"Foop",`))
	AssertIntsEqual(t, code, 403)
	AssertEqual(t, len(db.Boards[1][sha1Ofx].Boards), 0)

	_, code = ServeRequest(h, "POST", "/boards", fmt.Sprintf(updateStr, `"Password":"Foo",`))
	AssertIntsEqual(t, code, 200)
	testBoard := NewTestBoard(1)
	AssertEqual(t, db.Boards[1][sha1Ofx].Boards[0], testBoard)
}

func TestUpdateBoardsInvalid(t *testing.T) {
	h, db := NewTestHandlerNoPass()
	db.Sessions[1] = model.Session{SessionBase: model.SessionBase{
		Id: 1, Name: "Foo", Timestamp: 42}}
	db.Boards[1] = map[model.PublisherId]model.BoardSet{}

	updateStr := `{"SessionId": %d,
		"BoardSet":{"Team":"%s","PublisherId":"%s",
		"Boards":[{"BoardNumber":%d,"Suit":"%s","Level":%d,
		"Doubled":"%s","Tricks":%d,"Declarer":"%s","PublisherSeat":"%s"}]}}`

	ret, code := ServeRequest(h, "POST", "/boards", fmt.Sprintf(updateStr,
		0, "Teamx", "xxx", 1, model.Hearts, 7, model.Undoubled, 3, model.North, model.South))
	AssertIntsEqual(t, code, 400)
	AssertStringsEqual(t, ret, `{"Error":"Invalid SessionId"}`)

	ret, code = ServeRequest(h, "POST", "/boards", fmt.Sprintf(updateStr,
		2, "Teamx", "xxx", 1, model.Hearts, 7, model.Undoubled, 3, model.North, model.South))
	AssertIntsEqual(t, code, 404)
	AssertStringsEqual(t, ret, `{"Error":"No SessionId 2"}`)

	ret, code = ServeRequest(h, "POST", "/boards", fmt.Sprintf(updateStr,
		1, "Teamx", "", 1, model.Hearts, 7, model.Undoubled, 3, model.North, model.South))
	AssertIntsEqual(t, code, 400)
	AssertStringsEqual(t, ret, `{"Error":"Invalid PublisherId"}`)

	ret, code = ServeRequest(h, "POST", "/boards", fmt.Sprintf(updateStr,
		1, "", "xxx", 1, model.Hearts, 7, model.Undoubled, 3, model.North, model.South))
	AssertIntsEqual(t, code, 400)
	AssertStringsEqual(t, ret, `{"Error":"Team required"}`)

	invalidBoardStr := `{"Error":"Invalid`
	ret, code = ServeRequest(h, "POST", "/boards", fmt.Sprintf(updateStr,
		1, "Teamx", "xxx", -1, model.Hearts, 7, model.Undoubled, 3, model.North, model.South))
	AssertIntsEqual(t, code, 400)
	AssertStringsEqual(t, ret[:len(invalidBoardStr)], invalidBoardStr)

	invalidBoardStr = `{"Error":"Invalid`
	ret, code = ServeRequest(h, "POST", "/boards", fmt.Sprintf(updateStr,
		1, "Teamx", "xxx", 0, model.Hearts, 7, model.Undoubled, 3, model.North, model.South))
	AssertIntsEqual(t, code, 400)
	AssertStringsEqual(t, ret[:len(invalidBoardStr)], invalidBoardStr)

	ret, code = ServeRequest(h, "POST", "/boards", fmt.Sprintf(updateStr,
		1, "Teamx", "xxx", 99, "lksjdf", 7, model.Undoubled, 3, model.North, model.South))
	AssertIntsEqual(t, code, 400)
	AssertStringsEqual(t, ret[:len(invalidBoardStr)], invalidBoardStr)

	ret, code = ServeRequest(h, "POST", "/boards", fmt.Sprintf(updateStr,
		1, "Teamx", "xxx", 1, model.Hearts, 8, model.Undoubled, 3, model.North, model.South))
	AssertIntsEqual(t, code, 400)
	AssertStringsEqual(t, ret[:len(invalidBoardStr)], invalidBoardStr)

	ret, code = ServeRequest(h, "POST", "/boards", fmt.Sprintf(updateStr,
		1, "Teamx", "xxx", 1, model.Hearts, 3, "sdf", 3, model.North, model.South))
	AssertIntsEqual(t, code, 400)
	AssertStringsEqual(t, ret[:len(invalidBoardStr)], invalidBoardStr)

	ret, code = ServeRequest(h, "POST", "/boards", fmt.Sprintf(updateStr,
		1, "Teamx", "xxx", 1, model.Hearts, 8, model.Doubled, -1, model.North, model.South))
	AssertIntsEqual(t, code, 400)
	AssertStringsEqual(t, ret[:len(invalidBoardStr)], invalidBoardStr)

	ret, code = ServeRequest(h, "POST", "/boards", fmt.Sprintf(updateStr,
		1, "Teamx", "xxx", 1, model.Hearts, 2, model.Redoubled, 3, "", model.South))
	AssertIntsEqual(t, code, 400)
	AssertStringsEqual(t, ret[:len(invalidBoardStr)], invalidBoardStr)

	ret, code = ServeRequest(h, "POST", "/boards", fmt.Sprintf(updateStr,
		1, "Teamx", "xxx", 1, model.Hearts, 2, model.Redoubled, 3, model.East, "sdf"))
	AssertIntsEqual(t, code, 400)
	AssertStringsEqual(t, ret[:len(invalidBoardStr)], invalidBoardStr)

	AssertEqual(t, len(db.Boards[1][sha1Ofx].Boards), 0)
}
