package test

import (
	"bitbucket.org/tsiemens/bridgescoreserver/db"
	"bitbucket.org/tsiemens/bridgescoreserver/model"
)

type MockDb struct {
	NextSessionId model.SessionId
	Sessions      map[model.SessionId]model.Session
	Boards        map[model.SessionId]map[model.PublisherId]model.BoardSet
}

// Compile time assertion for interface implementation
var _ db.Database = (*MockDb)(nil)

func NewMockDb() *MockDb {
	return &MockDb{
		NextSessionId: 1,
		Sessions:      make(map[model.SessionId]model.Session),
		Boards:        make(map[model.SessionId]map[model.PublisherId]model.BoardSet),
	}
}

func (db *MockDb) Close() {}

func (db *MockDb) GetSessions() ([]model.Session, error) {
	var sessions []model.Session
	for _, v := range db.Sessions {
		sessions = append(sessions, v)
	}
	return sessions, nil
}

func (db *MockDb) GetSession(sessId model.SessionId) (
	session model.Session, ok bool, err error) {
	session, ok = db.Sessions[sessId]
	return session, ok, nil
}

func (db *MockDb) AddSession(sess *model.Session) error {
	sess.Id = db.NextSessionId
	db.NextSessionId += 1
	db.Sessions[sess.Id] = *sess
	return nil
}

func (db *MockDb) DelSession(sessId model.SessionId) error {
	delete(db.Sessions, sessId)
	delete(db.Boards, sessId)
	return nil
}

func (db *MockDb) GetSessionBoardSets(
	sessId model.SessionId) ([]model.BoardSet, error) {

	var boardSets []model.BoardSet
	for sid, boardSetMap := range db.Boards {
		if sid == sessId {
			for _, boardSet := range boardSetMap {
				boardSets = append(boardSets, boardSet)
			}
		}
	}
	return boardSets, nil
}

func (db *MockDb) AddOrUpdateBoardSet(
	boardSet *model.BoardSet, sessId model.SessionId) error {

	if _, ok := db.Boards[sessId]; !ok {
		db.Boards[sessId] = map[model.PublisherId]model.BoardSet{}
	}
	db.Boards[sessId][boardSet.PublisherId] = *boardSet
	return nil
}
