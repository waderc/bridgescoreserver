package test

import (
	"bitbucket.org/tsiemens/bridgescoreserver/util"
	"encoding/hex"
	_ "fmt"
	"testing"
)

func TestHashPassword(t *testing.T) {
	hash := util.HashPassword("mysalt", "password")
	AssertStringsEqual(t, hex.EncodeToString(hash[:]), "66be0390a0a2215721be51cf80e8c79aaab74a23")
}

func TestHexToHash(t *testing.T) {
	hash, err := util.HexToHash("66be0390a0a2215721be51cf80e8c79aaab74a23")
	AssertEqual(t, err, nil)
	AssertEqual(t, hash, util.HashPassword("mysalt", "password"))
}

func TestHexToHashErr(t *testing.T) {
	_, err := util.HexToHash("66be0390a0a2215721be51cf80e8c79aaab74a2")
	AssertTrue(t, err != nil)

	_, err = util.HexToHash("66be0390a0a2215721be51cf80e8c79aaab74a")
	AssertTrue(t, err != nil)

	_, err = util.HexToHash("66be0390a0a2215721be51cf80e8c79aaab74a2300")
	AssertTrue(t, err != nil)

	_, err = util.HexToHash("XX66be0390a0a2215721be51cf80e8c79aaab74a")
	AssertTrue(t, err != nil)
}
