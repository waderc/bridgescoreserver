package test

import (
	"fmt"
	"reflect"
	"runtime/debug"
	"testing"
)

func printBytes(s string) {
	for i := 0; i < len(s); i++ {
		fmt.Printf("%x ", s[i])
	}
	fmt.Printf("\n")
}

func AssertStringsEqual(t *testing.T, s string, e string) {
	if s != e {
		debug.PrintStack()
		// printBytes(s)
		// printBytes(e)
		t.Errorf("Strings not equal\nActual:  '%v'\nExpected:'%v'\n", s, e)
	}
}

func AssertIntsEqual(t *testing.T, i int, e int) {
	AssertEqual(t, i, e)
}

func AssertEqual(t *testing.T, v interface{}, e interface{}) {
	if v != e {
		debug.PrintStack()
		t.Errorf("Values not equal.\nActual:   %v\nExpected: %v\n", v, e)
	}
}

func AssertTrue(t *testing.T, b bool) {
	AssertEqual(t, b, true)
}

func AssertNil(t *testing.T, v interface{}) {
	AssertEqual(t, v, nil)
}

func AssertNotNil(t *testing.T, v interface{}) {
	if v == nil {
		debug.PrintStack()
		t.Errorf("Value was nil")
	}
}

func AssertDeepEqual(t *testing.T, v interface{}, e interface{}) {
	if !reflect.DeepEqual(v, e) {
		debug.PrintStack()
		t.Errorf("Values not equal.\nActual:   %+v\nExpected: %+v\n", v, e)
	}
}
